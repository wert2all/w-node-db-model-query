'use strict';

const ThrowableImplementationError = require('w-node-implementation-error');
/**
 * @class DBModelQueryImplementationError
 * @type {ThrowableImplementationError}
 */
module.exports = class DBModelQueryImplementationError
    extends ThrowableImplementationError {

};
