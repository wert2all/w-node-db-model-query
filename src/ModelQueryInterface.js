'use strict';

const DBModelQueryImplementationError = require('./Error/ImplementationError');

/**
 * @class DBModelQueryInterface
 * @type DBModelQueryInterface
 * @interface
 */
class DBModelQueryInterface {

    /**
     *
     * @param {[]} fields
     * @return {DBModelQueryInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setFields(fields) {
        throw new DBModelQueryImplementationError(this, 'setFields');
    }

    /**
     *
     * @param {number} offset
     * @param {number} limit
     * @return {DBModelQueryInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setLimit(offset, limit) {
        throw new DBModelQueryImplementationError(this, 'setLimit');
    }

    /**
     *
     * @param {string} field
     * @param {string} direction
     * @return {DBModelQueryInterface}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    addOrder(field, direction) {
        throw new DBModelQueryImplementationError(this, 'addOrder');
    }

    /**
     *
     * @param {string} field
     * @param {*} expression
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    addFilter(field, expression) {
        throw new DBModelQueryImplementationError(this, 'addFilter');
    }

    /**
     * @return {*}
     * @abstract
     */
    getQuery() {
        throw new DBModelQueryImplementationError(this, 'getQuery');
    }

    /**
     *
     * @param {DBModelRelationInterface} relation
     * @return {DBModelQueryInterface}
     * @abstract
     */
    addRelation(relation) {
        throw new DBModelQueryImplementationError(this, 'addRelation');
    }
}

module.exports = DBModelQueryInterface;
