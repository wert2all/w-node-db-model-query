'use strict';

/**
 * The entry point.
 *
 * @module DBModelQuery
 */
module.exports = require('./src/ModelQueryInterface');
